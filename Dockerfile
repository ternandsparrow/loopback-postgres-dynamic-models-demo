FROM node:12-slim
# don't use this for a production deploy, you should build the app once and make
# it part of the built image.

USER node
RUN mkdir -p /home/node/app
WORKDIR /home/node/app

# split the deps install from build for faster future builds
COPY --chown=node package.json yarn.lock ./
RUN yarn install --frozen-lockfile

ENV HOST=0.0.0.0 PORT=3000

EXPOSE ${PORT}
