import {inject, lifeCycleObserver, LifeCycleObserver} from '@loopback/core';
import {juggler} from '@loopback/repository';
import * as constants from '../constants'

const config = {
  name: constants.dataSourceName,
  connector: 'postgresql',
  url: '',
  host: getStringEnvVar('DB_HOST', 'localhost'),
  port: parseInt(getStringEnvVar('DB_PORT', '35432')),
  user: getStringEnvVar('DB_USER', 'user'),
  password: getStringEnvVar('DB_PASS', 'somepassword'),
  database: getStringEnvVar('DB_NAME', 'app')
};

// Observe application's life cycle to disconnect the datasource when
// application is stopped. This allows the application to be shut down
// gracefully. The `stop()` method is inherited from `juggler.DataSource`.
// Learn more at https://loopback.io/doc/en/lb4/Life-cycle.html
@lifeCycleObserver('datasource')
export class DemoApiDataSource extends juggler.DataSource
  implements LifeCycleObserver {
  static dataSourceName = constants.dataSourceName;
  static readonly defaultConfig = config;

  constructor(
    @inject(`datasources.config.${constants.dataSourceName}`, {optional: true})
    dsConfig: object = config,
  ) {
    super(dsConfig);
  }
}

function getStringEnvVar(varName:string, defaultValue:string):string {
  const result = process.env[varName]
  if (result) {
    return result
  }
  return defaultValue
}
