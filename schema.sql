DROP SCHEMA IF EXISTS public CASCADE;
DROP SCHEMA IF EXISTS vocabs CASCADE;

CREATE SCHEMA public;
CREATE SCHEMA vocabs;


CREATE TABLE vocabs.bike_type (
	code char(4) NOT NULL,
	name text,
  PRIMARY KEY (code)
);
INSERT INTO vocabs.bike_type VALUES ('MTB', 'Mountain bike');
INSERT INTO vocabs.bike_type VALUES ('ROAD', 'Road bike');

CREATE SEQUENCE public.bike_id_seq;

CREATE TABLE public.bike (
	id integer NOT NULL DEFAULT nextval('public.bike_id_seq'::regclass),
	name text,
  gears integer,
  bike_type char(4) REFERENCES vocabs.bike_type(code),
  PRIMARY KEY (id)
);

CREATE SEQUENCE public.person_id_seq;

CREATE TABLE public.person (
	id integer NOT NULL DEFAULT nextval('public.person_id_seq'::regclass),
	first_name text NOT NULL,
  surname text,
	favourite_bike integer REFERENCES public.bike(id),
  PRIMARY KEY (id)
);
